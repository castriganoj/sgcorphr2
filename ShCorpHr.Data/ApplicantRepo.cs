﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using SgCorpHr.Models;
using System.Web;

namespace SgCoprHr.Data
{
    public class ApplicantRepo
    {
        private string filePath = HostingEnvironment.MapPath(@"~/App_Data\Applicants.txt");


        public List<Applicant> GetAllApplicants()
        {
            List<Applicant> applicantList = new List<Applicant>();

            var reader = File.ReadAllLines(filePath);

            for (int i = 1; i < reader.Length; i++)
            {
                var columns = reader[i].Split(',');

                var applicant = new Applicant
                {
                    Id = int.Parse(columns[0]),
                    Name = columns[1],
                    Email = columns[2],
                    PhoneNumber = columns[3],
                    Address = columns[4],
                    City = (columns[5]),
                    State = columns[6],
                    ZipCode = columns[7],
                    Experience = columns[8],
                    Education = columns[9]
                };

                applicantList.Add(applicant);
            }

            return applicantList;
        }

        public Applicant CreateApplicant(Applicant a)
        {
            var applicantList = GenerateId(a);

            WriteToFile(applicantList);

            return a;
        }

        private void WriteToFile(List<Applicant> ApplicantList)
        {
            using (var writer = File.CreateText(filePath))
            {
                writer.WriteLine(
                    "Id,Name,Email,PhoneNumber,Address,City,State,ZipCode,Experience,Education");

                foreach (var Applicant in ApplicantList)
                {
                    writer.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", Applicant.Id, Applicant.Name,
                        Applicant.Email, Applicant.PhoneNumber, Applicant.Address, Applicant.City, Applicant.State,
                        Applicant.ZipCode, Applicant.Experience, Applicant.Education);
                }
            }
        }

        public List<Applicant> GenerateId(Applicant a)
        {

            var applicantList = new List<Applicant>();

            applicantList = GetAllApplicants();

            if (applicantList.Count < 1)
            {
                a.Id = 1;
            }
            else
            {
                a.Id = applicantList.Max(i => i.Id + 1);
            }

            applicantList.Add(a);

            return applicantList;
        }




    }
}
